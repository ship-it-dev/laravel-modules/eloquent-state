<?php


namespace ShipIT\EloquentState\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use ShipIT\EloquentState\Contracts\State as StateContract;
use ShipIT\EloquentState\Exceptions\ArgumentException;

class State implements CastsAttributes
{

    public function get($model, string $key, $value, array $attributes)
    {
        if (!$value) {
            return null;
        }

        return app($value);
    }

    public function set($model, string $key, $value, array $attributes)
    {
        if (!in_array(StateContract::class, class_implements($value))) {

            $className = is_string($value) ? $value : get_class($value);

            throw new ArgumentException(
                "{$className} must implement " . StateContract::class
            );
        }

        if (!is_object($value)) {
            return $value;
        }

        return get_class($value);
    }
}
