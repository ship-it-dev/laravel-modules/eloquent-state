<?php


namespace ShipIT\EloquentState\Models;

use Illuminate\Support\Str;

abstract class State implements \ShipIT\EloquentState\Contracts\State
{
    public function getAlias(): string
    {
        return Str::snake(class_basename($this));
    }
}
