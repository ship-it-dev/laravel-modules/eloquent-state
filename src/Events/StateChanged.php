<?php

namespace ShipIT\EloquentState\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;

class StateChanged
{
    use SerializesModels;

    private Model $model;

    public function __construct(
        Model $model
    )
    {
        $this->model = $model;
    }

    public function broadcastOn()
    {
        return [];
    }

    public function getModel(): Model
    {
        return $this->model;
    }
}
