<?php

namespace ShipIT\EloquentState\Providers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Arr;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->registerFactories();
        $this->registerMacros();

        if ($this->app->runningUnitTests()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../tests/Php/Utils/Database/Migrations');
        }
    }

    public function registerFactories()
    {
        if ($this->app->runningUnitTests()) {
            app(Factory::class)->load(__DIR__ . '/../../tests/Php/Utils/Database/factories');
        }
    }

    private function registerMacros(): void
    {
        Builder::macro('addStateFilter', function ($state): Builder {

            $state = Arr::wrap($state);

            return $this
                ->whereIn(
                    $this->getModel()->getStateColumn(),
                    $state
                );
        });
    }
}
