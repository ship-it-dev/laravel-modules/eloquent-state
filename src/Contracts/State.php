<?php


namespace ShipIT\EloquentState\Contracts;

interface State
{
    public function getAlias(): string;
}
