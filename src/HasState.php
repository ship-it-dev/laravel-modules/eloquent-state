<?php


namespace ShipIT\EloquentState;

use Illuminate\Database\Eloquent\Model;
use ShipIT\EloquentState\Contracts\State;
use ShipIT\EloquentState\Events\StateChanged;

/**
 * @property State|null state
 */
trait HasState
{

    public static function bootHasState()
    {
        self::saved(function (Model $model) {

            if (!$model->isDirty($model->getStateColumn())) {
                return;
            }

            event(new StateChanged($model));
        });
    }

    public function initializeHasState(): void
    {
        $this->mergeCasts([
            $this->getStateColumn() => Casts\State::class
        ]);
    }

    public function getStateColumn(): string
    {
        return 'state';
    }
}
