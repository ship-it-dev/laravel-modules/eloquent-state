<?php


namespace ShipIT\EloquentState\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use ShipIT\EloquentState\Contracts\State;
use ShipIT\EloquentState\Events\StateChanged;
use ShipIT\EloquentState\Tests\TestCase;
use ShipIT\EloquentState\Tests\Utils\Models\State\Other;
use ShipIT\EloquentState\Tests\Utils\Models\State\Some;
use ShipIT\EloquentState\Tests\Utils\Traits\CreatesTestModels;

class HasStatesTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function castsStateToModel(): void
    {
        $testModel = $this->makeTestModel([
            'state' => Some::class
        ]);

        self::assertInstanceOf(State::class, $testModel->state);
    }

    /**
     * @test
     */
    public function shouldFireEventWhenStateChanges(): void
    {
        $eventFake = Event::fake(StateChanged::class);

        $testModel = $this->makeTestModel();
        $testModel->state = Some::class;
        $testModel->save();

        $testModel->state = Other::class;
        $testModel->save();

        $eventFake->assertDispatchedTimes(StateChanged::class, 2);
    }
}
