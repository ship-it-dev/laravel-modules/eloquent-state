<?php


namespace ShipIT\EloquentState\Tests\Unit;


use Illuminate\Foundation\Testing\RefreshDatabase;
use ShipIT\EloquentState\Tests\TestCase;
use ShipIT\EloquentState\Tests\Utils\Models\State\Other;
use ShipIT\EloquentState\Tests\Utils\Models\State\Some;
use ShipIT\EloquentState\Tests\Utils\Models\TestModel;
use ShipIT\EloquentState\Tests\Utils\Traits\CreatesTestModels;

class BuilderTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    public function dataProvider(): array
    {
        return [
            'string' => [
                Some::class
            ],
            'array'  => [
                [Some::class]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider
     */
    public function addsMacroToBuilder($filterParam): void
    {
        $testModel = $this->createTestModel([
            'state' => Some::class
        ]);
        $this->createTestModel([
            'state' => Other::class
        ]);

        self::assertEquals(2, TestModel::query()->count());

        $collection = TestModel::query()->addStateFilter($filterParam)->get();

        self::assertCount(1, $collection);
        self::assertTrue($collection->contains($testModel));
    }
}