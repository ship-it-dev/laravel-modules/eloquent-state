<?php


namespace ShipIT\EloquentState\Tests\Utils\Traits;

use ShipIT\EloquentState\Contracts\State;
use ShipIT\EloquentState\Tests\Utils\Models\TestModel;

/**
 * @property State
 */
trait CreatesTestModels
{
    protected function makeTestModel(array $attributes = []): TestModel
    {
        return factory(TestModel::class)->make($attributes);
    }

    protected function createTestModel(array $attributes = []): TestModel
    {
        return factory(TestModel::class)->create($attributes);
    }
}

