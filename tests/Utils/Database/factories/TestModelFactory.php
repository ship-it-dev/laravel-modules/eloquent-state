<?php
/**
 * @var Factory $factory
 */
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use ShipIT\EloquentState\Tests\Utils\Models\State\Some;
use ShipIT\EloquentState\Tests\Utils\Models\TestModel;

$factory->define(TestModel::class, function (Faker $faker) {
    return [
        'state' => Some::class
    ];
});
