<?php


namespace ShipIT\EloquentState\Tests\Utils\Models;

use Illuminate\Database\Eloquent\Model;
use ShipIT\EloquentState\HasState;

class TestModel extends Model
{
    use HasState;
}
